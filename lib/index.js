const camelCase = require('lodash.camelcase')
const debug = require('debug')('j2i:extract')
const Joi = require('joi')
const { basename, join } = require('path')

const inputObjectSchema = require('../models/input-object-schema')
const metaSchema = require('../models/metaschema')
const reducerSchema = require('../models/reducer-schema')

function loadReducers () {
  return require('require-all')({
    dirname: join(__dirname, 'reducers'),
    filter: new RegExp('^((?!\\.spec\\.js).)*$', 'g'),
    map: (name) => camelCase(basename(name, '.js'))
  })
}

function extract (joiSchema = {}, reducers = loadReducers()) {
  debug('available reducers', Object.keys(reducers))
  const schema = Joi.attempt(joiSchema, metaSchema)
  debug('schema validated', schema)
  let { _inner: { children } = {} } = schema
  children = children || []
  debug(`${children.length} available children`, children)

  return children.map(({ key }) => {
    debug(`# key ${key}`)
    const initialInput = {
      name: key,
      attributes: {}
    }
    const keySchema = Joi.reach(schema, key)
    debug(`${key}'s schema`, keySchema)

    return Object.values(reducers)
      .reduce(
        (inputObj, reducerFn) => {
          Joi.assert(reducerFn, reducerSchema)
          debug('assert ok, will execute reducer', reducerFn)
          return Joi.attempt(reducerFn(inputObj, keySchema), inputObjectSchema)
        },
        initialInput
      )
  })
}

module.exports = extract
