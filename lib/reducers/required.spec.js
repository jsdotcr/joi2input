/* eslint no-unused-expressions: 0 */
const chai = require('chai')
const { beforeEach, afterEach, describe, it } = require('mocha')
const sinon = require('sinon')

chai.use(require('sinon-chai'))
chai.config.includeStack = true

const { expect } = chai
const requiredReducer = require('./required')
const Joi = require('joi')

describe('required reducer', () => {
  let sandbox

  beforeEach(() => {
    sandbox = sinon.sandbox.create()
  })
  afterEach(() => {
    sandbox.restore()
  })

  describe('schema validation', () => {
    it('calls joi.assert for the first input param', () => {
      const assertStub = sandbox.stub(Joi, 'assert')
      const firstArgument = {
        name: '13',
        attributes: {}
      }
      const secondArgument = Joi.object().keys({})
      requiredReducer(firstArgument, secondArgument)
      expect(assertStub).to.have.been.calledOnce
      expect(assertStub.getCall(0).args[0]).to.deep.equal(firstArgument)
    })
    it('throws if second param is not a joi type', () => {
      const firstArgument = {
        name: '13',
        attributes: {}
      }
      const secondArgument = Joi.any()
      expect(() => {
        requiredReducer(firstArgument, secondArgument)
      }).not.to.throw('Not a valid key subschema')
      expect(() => {
        requiredReducer(firstArgument, {})
      }).to.throw('Not a valid key subschema')
    })
  })

  describe('"presence" flag', () => {
    const inputObject = {
      name: 'aName',
      attributes: {}
    }

    describe('with a string type', () => {
      it('returns false with an optional field', () => {
        const output = requiredReducer(inputObject, Joi.string().optional())
        expect(output.attributes.required).to.be.false
      })
      it('returns false with an unset presence', () => {
        const output = requiredReducer(inputObject, Joi.string())
        expect(output.attributes.required).to.be.false
      })
      it('returns true with a required field', () => {
        const output = requiredReducer(inputObject, Joi.string().required())
        expect(output.attributes.required).to.be.true
      })
    })
  })
})
