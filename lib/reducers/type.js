const debug = require('debug')('j2i:reducers:type')
const Joi = require('joi')
const kebabCase = require('lodash.kebabcase')
const inputObjectSchema = require('../../models/input-object-schema')

const typesList = [
  function email ({ _type, _tests }) {
    debug('tests', _tests)
    debug(_tests.some(({ name }) => name === 'email'))
    return _type === 'string' && _tests.some(({ name }) => name === 'email')
  },
  function url ({ _type, _tests }) {
    debug('tests', _tests)
    debug(_tests.some(({ name }) => name === 'uri'))
    return _type === 'string' && _tests.some(({ name }) => name === 'uri')
  },
  function datetimeLocal ({ _type, _flags }) {
    debug('flags', _flags)
    return _type === 'date' && (_flags.format || _flags.timestamp)
  },
  function date ({ _type, _tests }) {
    debug('type', _type)
    return _type === 'date' || (_type === 'string' && _tests.some(({ name }) => name === 'isoDate'))
  }
]

function typeReducer (inputObject, keySchema) {
  Joi.assert(inputObject, inputObjectSchema)
  if (!keySchema.isJoi) {
    throw new Error('Not a valid key subschema')
  }

  debug('key schema', keySchema)
  let type
  for (const typeFn of typesList) {
    if (typeFn(keySchema)) {
      type = kebabCase(typeFn.name)
      break
    }
  }

  inputObject.attributes.type = type || 'text'

  return inputObject
}

module.exports = typeReducer
