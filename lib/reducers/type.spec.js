/* eslint no-unused-expressions: 0 */
const chai = require('chai')
const { beforeEach, afterEach, describe, it } = require('mocha')
const sinon = require('sinon')

chai.use(require('sinon-chai'))

const { expect } = chai
const typeReducer = require('./type')
const Joi = require('joi')

describe('type reducer', () => {
  let sandbox

  beforeEach(() => {
    sandbox = sinon.sandbox.create()
  })
  afterEach(() => {
    sandbox.restore()
  })

  describe('schema validation', () => {
    it('calls joi.assert for the first input param', () => {
      const assertStub = sandbox.stub(Joi, 'assert')
      const firstArgument = {
        name: '13',
        attributes: {}
      }
      const secondArgument = Joi.object().keys({})
      typeReducer(firstArgument, secondArgument)
      expect(assertStub).to.have.been.calledOnce
      expect(assertStub.getCall(0).args[0]).to.deep.equal(firstArgument)
    })
    it('throws if second param is not a joi type', () => {
      const firstArgument = {
        name: '13',
        attributes: {}
      }
      const secondArgument = Joi.any()
      expect(() => {
        typeReducer(firstArgument, secondArgument)
      }).not.to.throw('Not a valid key subschema')
      expect(() => {
        typeReducer(firstArgument, {})
      }).to.throw('Not a valid key subschema')
    })
  })

  describe('match an email', () => {
    const inputObject = {
      name: 'email',
      attributes: {}
    }

    it('returns the type="email" when using Joi.string().email()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string().email())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type', 'email')
    })
    it('does not return type="email" when using Joi.string()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type').not.equal('email')
    })
    it('does NOT return type="email" when using an email-like pattern', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string().regex(/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/))
      expect(returningInputObject).to.have.property('attributes').to.have.property('type').not.equal('email')
    })

    it('discards any option', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string().email({
        errorLevel: 42,
        tldWhitelist: ['com'],
        minDomainAtoms: 42
      }))
      expect(returningInputObject).to.have.property('attributes').to.have.all.keys('type')
    })
  })

  describe('match a URL', () => {
    const inputObject = {
      name: 'url',
      attributes: {}
    }

    it('returns the type="url" when using Joi.string().uri()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string().uri())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type', 'url')
    })
    it('does not return type="url" when using Joi.string()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type').not.equal('email')
    })
    it('does NOT return type="uri" when using an url-like pattern', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string().regex(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/))
      expect(returningInputObject).to.have.property('attributes').to.have.property('type').not.equal('url')
    })

    it('discards any option', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string().uri({
        scheme: ['https'],
        allowRelative: true,
        relativeOnly: false
      }))
      expect(returningInputObject).to.have.property('attributes').to.have.all.keys('type')
    })
  })

  describe('match a date', () => {
    const inputObject = {
      name: 'date',
      attributes: {}
    }

    it('returns the type="date" when using Joi.date()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.date())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type', 'date')
    })
    it('returns the type="date" when using Joi.string().isoDate()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.string().isoDate())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type', 'date')
    })
    it('returns type="datetime-local" when using Joi.date().iso()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.date().iso())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type', 'datetime-local')
    })
    it('returns type="datetime-local" when using Joi.date().timestamp()', () => {
      const returningInputObject = typeReducer(inputObject, Joi.date().timestamp())
      expect(returningInputObject).to.have.property('attributes').to.have.property('type', 'datetime-local')
    })
  })
})
