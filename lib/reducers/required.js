const debug = require('debug')('j2i:reducers:required')
const Joi = require('joi')
const inputObjectSchema = require('../../models/input-object-schema')

function required (inputObject, keySchema) {
  Joi.assert(inputObject, inputObjectSchema)
  if (!keySchema.isJoi) {
    throw new Error('Not a valid key subschema')
  }
  debug('will check presence in the flags hash', keySchema)
  inputObject.attributes.required = keySchema._flags.presence === 'required'
  return inputObject
}

module.exports = required
