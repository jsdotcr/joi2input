/* eslint no-unused-expressions: 0 */
const { beforeEach, afterEach, describe, it } = require('mocha')
const chai = require('chai')
const proxyquire = require('proxyquire').noPreserveCache()
const sinon = require('sinon')

chai.use(require('sinon-chai'))
chai.config.includeStack = true

const { expect } = chai
const Joi = require('joi')

describe('extract', function () {
  let sandbox

  beforeEach(() => {
    sandbox = sinon.sandbox.create()
  })
  afterEach(() => {
    sandbox.restore()
  })

  describe('empty/invalid schema', function () {
    const extract = require('.')
    it('should return an empty object when undefined', () => {
      const inputObject = extract()
      expect(Object.keys(inputObject)).to.have.lengthOf(0)
    })
  })

  describe('when type is not object', () => {
    const extract = require('.')
    it('throws if it is an array', () => {
      expect(() => {
        extract(Joi.array())
      }).to.throw(Error).and.to.have.property('name', 'ValidationError')
    })
    it('throws if it is a string', () => {
      expect(() => {
        extract(Joi.string())
      }).to.throw(Error).and.to.have.property('name', 'ValidationError')
    })
    it('throws if it is a date', () => {
      expect(() => {
        extract(Joi.date())
      }).to.throw(Error).and.to.have.property('name', 'ValidationError')
    })
    it('throws if it is a boolean', () => {
      expect(() => {
        extract(Joi.boolean())
      }).to.throw(Error).and.to.have.property('name', 'ValidationError')
    })
    it('throws if it is a binary', () => {
      expect(() => {
        extract(Joi.binary())
      }).to.throw(Error).and.to.have.property('name', 'ValidationError')
    })
    it('throws if it is a function', () => {
      expect(() => {
        extract(Joi.func())
      }).to.throw(Error).and.to.have.property('name', 'ValidationError')
    })
    it('throws if it is a number', () => {
      expect(() => {
        extract(Joi.number())
      }).to.throw(Error).and.to.have.property('name', 'ValidationError')
    })
  })

  describe('when type is an object', () => {
    const extract = require('.')
    describe('returns an empty list', () => {
      it('uses an empty object as input', () => {
        const inputObject = extract({})
        expect(Object.keys(inputObject)).to.have.lengthOf(0)
      })
      it('uses an empty Joi schema as input', () => {
        const inputObject = extract(Joi.object())
        expect(Object.keys(inputObject)).to.have.lengthOf(0)
      })
      it('uses a Joi schema with empty set of keys as input', () => {
        const inputObject = extract(Joi.object().keys({}))
        expect(Object.keys(inputObject)).to.have.lengthOf(0)
      })
    })
    it('returns a list with 1 element when one key is defined', () => {
      const keys = {
        aRandomName: Joi.any()
      }
      const inputObject = extract(Joi.object().keys(keys), [])
      console.log('out', inputObject)
      expect(Object.keys(inputObject)).to.have.lengthOf(Object.keys(keys).length)
    })
  })

  describe('initial input', () => {
    const extract = require('.')
    const keyName = 'hello'
    const mockSchema = Joi.object().keys({
      [keyName]: Joi.string()
    })
    it('has a key "name"', () => {
      const reducers = {
        fakeReducer (initialInput, _) {
          expect(initialInput).to.be.an('object')
            .and.to.have.property('name', keyName)
        }
      }
      extract(mockSchema, reducers)
    })
    it('has a key "attributes"', () => {
      const reducers = {
        fakeReducer (initialInput, _) {
          expect(initialInput).to.be.an('object')
            .and.to.have.property('attributes').that.is.an('object')
        }
      }
      extract(mockSchema, reducers)
    })
  })

  describe('reducers', () => {
    const mockSchemaKey = 'hey'
    const mockSchema = Joi.object().keys({
      [mockSchemaKey]: Joi.string()
    })
    describe('mocked require-all', () => {
      let extract
      let requireAllStub
      beforeEach(() => {
        requireAllStub = sandbox.stub().returns([])
        extract = proxyquire('.', {
          'require-all': requireAllStub
        })
      })
      it('calls require-all', () => {
        extract(mockSchema)
        expect(requireAllStub).to.have.been.calledOnce
      })
      it('filters out test files', () => {
        extract(mockSchema)
        const { filter } = requireAllStub.getCall(0).args[0]
        expect(filter.test('hello.spec.js'), '.spec.js').to.be.false
        expect(filter.test('hello.js'), 'nested .js').to.be.true
      })
    })
    describe('execution', () => {
      const extract = require('.')
      it('throws if reducer does not respect the reducer schema', () => {
        expect(() => {
          extract(mockSchema, [
            function () {}
          ])
        }, 'no params').to.throw(Error).and.to.have.property('name', 'ValidationError')
        expect(() => {
          extract(mockSchema, [
            function (oneParam) {}
          ])
        }, 'one param').to.throw(Error).and.to.have.property('name', 'ValidationError')
        expect(() => {
          extract(mockSchema, [
            function (oneParam, twoParams) {}
          ])
        }, 'two params').not.to.throw(Error)
        expect(() => {
          extract(mockSchema, [
            function (oneParam, twoParams, threeParams) {}
          ])
        }, 'three params').to.throw(Error)
      })
      it('extracts the key schema', () => {
        const reachSpy = sandbox.spy(Joi, 'reach')
        extract(mockSchema, [])
        expect(reachSpy.getCall(0).args[0].isJoi).to.equal(true)
        expect(reachSpy.getCall(0).args[0]._type).to.equal('object')
        expect(reachSpy.getCall(0).args[0]._inner.children[0].key).to.equal(mockSchemaKey)
        expect(reachSpy.getCall(0).args[1]).to.equal(mockSchemaKey)
      })
      describe('validationError on invalid input object schema', () => {
        it('throws on missing key/attributes properties', () => {
          const firstSpy = sinon.spy()
          const secondSpy = sinon.spy(0)
          expect(() => {
            extract(mockSchema, [
              function (previous, keySchema) {
                firstSpy()
                return {
                  invalid: 'schema'
                }
              },
              function (previous, keySchema) {
                secondSpy()
              }
            ])
          }).to.throw(Error).and.to.have.property('name', 'ValidationError')
          expect(firstSpy).to.have.been.calledOnce
          expect(secondSpy).to.have.not.been.called
        })
        it('throws on non-whitelisted attribute', () => {
          expect(() => {
            extract(mockSchema, [
              function (previous, keySchema) {
                return {
                  name: 'look at the attrs',
                  attributes: {
                    notWhitelisted: true
                  }
                }
              }
            ])
          }).to.throw(Error).and.to.have.property('name', 'ValidationError')
        })
      })
      it('returns the reducer\'s output', () => {
        const firstInput = {
          name: 'first',
          attributes: {
            required: false
          }
        }
        const secondInput = {
          name: 'second',
          attributes: {
            required: true
          }
        }
        const final = extract(mockSchema, [
          function (previous, keySchema) {
            return firstInput
          },
          function (previous, keySchema) {
            expect(previous).to.deep.equal(firstInput)
            return secondInput
          }
        ])

        expect(final[0]).to.deep.equal(secondInput)
      })
    })
  })
})
