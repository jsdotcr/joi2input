const Joi = require('joi')

const inputObjectSchema = Joi.object()
  .keys({
    name: Joi.string().required(),
    attributes: Joi.object().keys({
      required: Joi.boolean(),
      type: Joi.string().valid(['email', 'datetime-local', 'date', 'url', 'text'])
      // doable: 'number', 'range', 'tel',
      // maybes: 'radio', 'color', 'image', 'checkbox', 'password', 'time'
      // excluded: X'button', X'datetime', X'file', X'month', X'reset', X'search', X'submit', X'week', 'hidden'
    })
  })

module.exports = inputObjectSchema
