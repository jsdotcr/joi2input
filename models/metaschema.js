const Joi = require('joi')

const metaSchema = Joi.object()
  .keys({
    isJoi: Joi.boolean().valid(true),
    _inner: Joi.object().keys({
      children: Joi.alternatives().try(Joi.array(), null).required()
    }),
    _type: Joi.string().valid(['object']),
    _flags: Joi.object().keys({
      func: Joi.boolean().falsy().forbidden()
    }).unknown(true)
  })
  .options({
    stripUnknown: true
  })

module.exports = metaSchema
