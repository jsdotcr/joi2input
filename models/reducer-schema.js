const Joi = require('joi')

const reducerSchema = Joi.func()
  .arity(2)

module.exports = reducerSchema
